#include <stdio.h>
#include <unistd.h>

#include "avl.h"
#include "queue.h"

void preorder(struct avlnode *root)
{
	if (!root)
		return;

	preorder(root->left);
	printf("%d ", root->key);
	preorder(root->right);

	return;
}

void inorder(struct avlnode *root)
{
	if (!root)
		return;

	printf("%d ", root->key);
	inorder(root->left);
	inorder(root->right);

	return;
}

void postorder(struct avlnode *root)
{
	if (!root)
		return;

	postorder(root->left);
	postorder(root->right);
	printf("%d ", root->key);

	return;
}

void levelorder(struct avlnode *root)
{
    struct qhead *head;
    struct avlnode *tmp;

    head = createqueue();

    tmp = root;
    while (tmp) {    
        // print
        printf("%d ", tmp->key);

        // enqueue children
        if (tmp->left)
            enqueue(head, tmp->left);

        if (tmp->right)
            enqueue(head, tmp->right);

        // dequeue node
        tmp = dequeue(head);
    }   
}
