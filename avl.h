#define EH	0
#define LH	-1
#define RH	1

struct avlnode { 
	int key;
	struct avlnode *left;
	struct avlnode *right;
	int bal;
};

struct avlnode *_avlinsert(struct avlnode *root, struct avlnode *new);
struct avlnode *leftbalance(struct avlnode *root);
struct avlnode *rightbalance(struct avlnode *root);
struct avlnode *rotateleft(struct avlnode *root);
struct avlnode *rotateright(struct avlnode *root);
struct avlnode *avlremove(struct avlnode *root, int key);
struct avlnode *deleteleftbalance(struct avlnode *root);
struct avlnode *deleterightbalance(struct avlnode *root);
struct avlnode *avlinsert(struct avlnode *root, int key);
