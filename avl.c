#include <stdio.h>
#include <stdlib.h>

#include "avl.h"

#define FALSE 0
#define TRUE 1

// insert 시, 모든 되부름 스택들이 트리변화를 알아야 한다. --> global
// g_is_taller 가 TRUE 가 되는 경우 : 리프노드에 삽입시
// g_is_taller 가 FALSE 가 되는 경우 : LH-RH 나 RH-LH 가 EH 될때, 또는 balance 함수 탈때
int g_is_taller = FALSE;

// delete 시, 모든 되부름 스택들은 트리변화를 알아야 한다. --> global
int g_is_shorter = FALSE;

// 래퍼
struct avlnode *avlinsert(struct avlnode *root, int key)
{
	struct avlnode *new = calloc(sizeof(struct avlnode), 1);
	if (!new) {
		printf("create new node fail\n");
		return NULL;
	}

	new->key = key;

	root = _avlinsert(root, new);
	return root;
}

// 알고리즘
struct avlnode *_avlinsert(struct avlnode *root, struct avlnode *new)
{
    // root 가 null (리프노드) : new 를 root 로 삽입
    if (!root) {
		g_is_taller = TRUE;	
		return new;
    }   

	// BST 처럼 삽입 --> g_is_taller 값 확인 --> TRUE 면 root bal 값에 따라 분기
	if (new->key < root->key) {
		// 왼쪽 부분트리가 높아짐
		root->left = _avlinsert(root->left, new);

		// root balance 조정
		if (g_is_taller) {
			switch (root->bal) {
				case LH:	
					root = leftbalance(root);
					break;
				case EH:
					root->bal = LH;
					break;
				case RH:
					root->bal = EH;
					g_is_taller = FALSE;
					break;
				default:
					printf("unknown balance value\n");
					break;
			}
		}
	} else {
		// 오른쪽 부분트리가 높아짐
		root->right = _avlinsert(root->right, new);

		// root balance 조정
		if (g_is_taller) {
			switch (root->bal) {
				case LH:
					root->bal = EH;
					g_is_taller = FALSE;
					break;
				case EH:
					root->bal = RH;
					break;
				case RH:
					root = rightbalance(root);
					break;
				default:
					printf("unknown balance value\n");
					break;
			}
		}
	}

	// 밸런스 조정 끝난 root 리턴
	return root;
}

// root 의 왼족 트리 높이 > root 의 오른쪽 트리 높이
struct avlnode *leftbalance(struct avlnode *root)
{
	// 회전전에 백업
	struct avlnode *oldroot = root;
	struct avlnode *oldleft = root->left;	

	// EH 는 존재하지 않음
	if (oldleft->bal == LH) {		// LH-LH 인 경우, root 를 오른쪽 회전
		// 회전
		root = rotateright(root);		

		// 밸런스 업데이트
		oldroot->bal = EH;		
		oldleft->bal = EH;	
		g_is_taller = FALSE;
	} else {						// LH-RH 인 경우, root->left 를 왼쪽 회전, root 를 오른쪽 회전

		// 회전전에 백업
		struct avlnode *oldright = oldleft->right;

		// 회전
		root->left = rotateleft(root->left);
		root = rotateright(root);
		g_is_taller = FALSE;

		// 밸런스 업데이트
		if (oldright->bal == LH) {
			oldroot->bal = RH;			//// point
			oldleft->bal = EH;
		} else if (oldright->bal == EH) {
			oldleft->bal = EH;
		} else {
			oldroot->bal = EH;
			oldleft->bal = LH;
		}
	}

	// 밸런스 조정 끝난 root 리턴
	return root;
}

// root 의 왼족 트리 높이 < root 의 오른쪽 트리 높이
struct avlnode *rightbalance(struct avlnode *root)
{
	struct avlnode *oldroot = root;
	struct avlnode *oldright = root->right;

	if (oldright->bal == LH) {		// RH-LH 인 경우
		struct avlnode *oldleft = oldright->left;

		// 회전
		root->right = rotateright(root->right);
		root = rotateleft(root);
		g_is_taller = FALSE;

		// 밸런스 업데이트	
		if (oldleft->bal == RH) {
			oldroot->bal = LH;			//// point
			oldright->bal = EH;
		} else if (oldleft->bal == EH) {
			oldright->bal = EH;
		} else {
			oldroot->bal = EH;
			oldright->bal = RH;
		}
	} else {						// RH-RH 인 경우
		root = rotateleft(root);

		oldroot->bal = EH;
		oldright->bal = EH;
		g_is_taller = FALSE;		
	}

	return root;
}

// rotateleft : root->right 가 새로운 root, root->right->left 는 oldroot 의 right 로 삽입 
// 호출자 쪽에서 새로운 root 를 가리키게 세팅
struct avlnode *rotateleft(struct avlnode *root)
{
	struct avlnode *oldroot = root;
	struct avlnode *newroot = root->right;

	// 순서중요
	oldroot->right = newroot->left;
	newroot->left = oldroot;

	return newroot;
}

// rotateright : root->left 가 새로운 root, root->left->right 는 oldroot 의 left 로 삽입
// 호출자 쪽에서 새로운 root 를 가리키게 세팅
struct avlnode *rotateright(struct avlnode *root)
{
	struct avlnode *oldroot = root;
	struct avlnode *newroot = root->left;

	// 순서중요
	oldroot->left = newroot->right;
	newroot->right = oldroot;

	return newroot;	
}

struct avlnode *avlremove(struct avlnode *root, int key)
{
	struct avlnode *delnode;
	struct avlnode *newroot;			// delnode free 후 리턴할 root, delnode 의 자식

	if (!root) {
		return NULL;
	}

	// 삭제 --> 부모에서 balance 조정
	if (key < root->key) {
		root->left = avlremove(root->left, key);

		// left subtree 제거후 높이변화 발생시, rightbalance 수행
		if (g_is_shorter)
			root = deleterightbalance(root);
	} else if (key > root->key) {
		root->right = avlremove(root->right, key);

		// right subtree 제거후 높이변화 발생시, leftbalance 수행
		if (g_is_shorter)
			root = deleteleftbalance(root);
	} else {
		delnode = root;

		// leaf 노드인지 체크
		if ((!delnode->left) && (!delnode->right)) {
			g_is_shorter = TRUE;
			free(delnode);
			return NULL;
		} else if (!delnode->right) {
			newroot = delnode->left;
			g_is_shorter = TRUE;
			free(delnode);
			return newroot;
		} else if(!delnode->left) {
			newroot = delnode->right;
			g_is_shorter = TRUE;
			free(delnode);
			return newroot;
		} else {
			// both subtree 인 경우, left subtree의 rightmost 노드를 이용
			struct avlnode *leftsub;
			struct avlnode *rightmost;

			leftsub = root->left;
			while (leftsub->right) {
				leftsub = leftsub->right;
			}

			rightmost = leftsub;			
			root->key = rightmost->key;

			// rightmost 는 leaf 이므로, balance 필요 ( 여기선 rightbalance 를 쓰기로 약속 )
			root->left = avlremove(root->left, rightmost->key);

			if (g_is_shorter)
				root = deleterightbalance(root);
		}
	}

	return root;
}

// right subtree 제거, 높이변화 발생
struct avlnode *deleteleftbalance(struct avlnode *root)
{
	// LH 인 경우에만 이용
	struct avlnode *oldroot = root;
	struct avlnode *oldleft = root->left;

	// 분기
	if (root->bal == LH) {

		if (oldleft->bal == RH) {		// LH-RH : 2번 회전 필요
			struct avlnode *oldright = oldleft->right;

			root->left = rotateleft(root->left);
			root = rotateright(root);

			// balance update
			if (oldright->bal == LH) {
				oldleft->bal = LH;
				oldroot->bal = EH;
			} else if (oldright->bal == EH) {
				oldleft->bal = EH;
				oldroot->bal = EH;
			} else {
				oldleft->bal = EH;
				oldroot->bal = RH;
			}

			oldright->bal = EH;
		} else {
			root = rotateright(root);

			// balance update
			if (oldleft->bal != EH) {
				oldleft->bal = EH;
				oldroot->bal = EH;
			} else {
				oldleft->bal = RH;
				oldroot->bal = LH;
				g_is_shorter = FALSE;
			}
		} 
	} else if (root->bal == EH) {
		root->bal = LH;
		g_is_shorter = FALSE;
	} else {
		root->bal = EH;
	}	

	return root;
}

// left subtree 제거, 높이변화 발생
struct avlnode *deleterightbalance(struct avlnode *root)
{
	// RH 인 경우에만 이용
	struct avlnode *oldroot = root;
	struct avlnode *oldright = root->right;

	// root 상태에 따라 분기
	if (root->bal == LH) {
		root->bal = EH;			// 전체트리에서 g_is_shorter 인지는 알수없으므로, 스킵
	} else if (root->bal == EH) {
		root->bal = RH;
		g_is_shorter = FALSE;	// 기존에 EH 였으므로, no need to balance further
	} else {		// RH

		if (oldright->bal == LH) {		// RH-LH : 2번 회전필요
			struct avlnode *oldleft = oldright->left;

			root->right = rotateright(root->right);				
			root = rotateleft(root);

			// balance update 
			if (oldleft->bal == RH) {
				oldright->bal = RH;
				oldroot->bal = EH;
			} else if (oldleft->bal == EH) {
				oldright->bal = EH;
				oldroot->bal = EH;
			} else {
				oldright->bal = EH;
				oldroot->bal = LH;
			}

			oldleft->bal = EH;
		} else {						// RH-EH, RH-RH : 1번 회전필요
			root = rotateleft(root);

			// balance update
			if (oldright->bal != EH) {		// RH-RH
				oldright->bal = EH;
				oldroot->bal = EH;			// 전체트리에서 g_is_shorter 인지는 알수없으므로, 스킵
			} else {						// RH-EH
				oldright->bal = LH;
				oldroot->bal = RH;			// already RH
				g_is_shorter = FALSE;		// EH 였으므로, 밸런스 맞춰짐
			}
		}
	}

	return root;
}
