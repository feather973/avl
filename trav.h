void preorder(struct avlnode *root);
void inorder(struct avlnode *root);
void postorder(struct avlnode *root);
void levelorder(struct avlnode *root);
