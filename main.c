#include <stdio.h>
#include <stdlib.h>

#include "avl.h"
#include "trav.h"

int main()
{
	struct avlnode *root = NULL;

	root = avlinsert(root, 10);
	root = avlinsert(root, 20);
	root = avlinsert(root, 30);
	root = avlinsert(root, 100);
	root = avlinsert(root, 90);
	root = avlinsert(root, 80);
	root = avlinsert(root, 40);
	root = avlinsert(root, 60);
	root = avlinsert(root, 59);
	root = avlinsert(root, 57);
	root = avlinsert(root, 51);
	root = avlinsert(root, 50);
	root = avlinsert(root, 9);
	root = avlinsert(root, 1000);
	root = avlinsert(root, 10000);
	root = avlinsert(root, 100000);
	root = avlinsert(root, 1000000);

	preorder(root);
//	inorder(root);
//	postorder(root);

	printf("\nlevel test\n");
	levelorder(root);

	root = avlremove(root, 59);
	root = avlremove(root, 60);
	root = avlremove(root, 40);

	printf("\n\nlevel test : remove 59 60 40\n");
	levelorder(root);

	root = avlremove(root, 10);
	root = avlremove(root, 20);
	root = avlremove(root, 100000);
	root = avlremove(root, 30);

	printf("\n\nlevel test : remove 10 20 100000 30\n");
	levelorder(root);

	root = avlremove(root, 90);
	root = avlremove(root, 1000000);
	root = avlremove(root, 80);
	root = avlremove(root, 51);

	printf("\n\nlevel test : remove 90 1000000 80 51\n");
	levelorder(root);

	root = avlremove(root, 100);
	root = avlremove(root, 50);
	root = avlremove(root, 57);

	printf("\n\nlevel test : remove 100 50 57\n");
	levelorder(root);

	root = avlremove(root, 1000);
	root = avlremove(root, 10000);
	root = avlremove(root, 9);

	printf("\n\nlevel test : empty\n");
	levelorder(root);

	return 0;
}
